#!/bin/bash
set -ex

device="$1"
lunch="$2"
ver="$3"
iscombined="$4"

#Optional partitions to be built
makedtbo="$5"
makevbmeta="$6"
makevendor="$7"

if [ "$iscombined" = "true" ]; then
    bootimage_target=bootimage
else
    bootimage_target=halium-boot
fi

cd "/opt/halium_build/$ver"
rm -rf ./out/

if [ -d "hybris-patches" ]; then
    hybris-patches/apply-patches.sh --mb
fi

export LC_ALL=C
export USE_CCACHE=1
source build/envsetup.sh
lunch "$lunch-userdebug"

if [[ "$device" == halium_* ]]; then
    time make -j10 recoveryimage systemimage
elif [ "$ver" = "9.0" ]; then
    time make -j10 $bootimage_target recoveryimage systemimage
    if [ "$makedtbo" = "true" ]; then
        time make -j10 dtboimage
    fi
    if [ "$makevbmeta" = "true" ]; then
        time make -j10 vbmetaimage
    fi
    if [ "$makevendor" = "true" ]; then
        time make -j10 vendorimage
    fi
else
    time make -j10
    time make halium-boot -j10
fi

mkdir "./out/target/product/$device/images"
mv ./out/target/product/"$device"/*.img "./out/target/product/$device/images/"

if [ "$iscombined" != "true" ]; then
    lunch "$lunch-eng"
    time make recoveryimage -j10
    mv "./out/target/product/$device/recovery.img" "./out/target/product/$device/images/recovery-unlocked.img"
fi
