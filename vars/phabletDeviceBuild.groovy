def call(String deviceName, String lunchName, String deviceDir) {
  String agentTag = "phablet-5.1"
  String artifacts = 'device_*_devel.tar.*,ubp*'

  pipeline {
    agent { label "$agentTag" }
    options {
      skipDefaultCheckout()
      buildDiscarder(logRotator(artifactNumToKeepStr: '30'))
      throttle(["$agentTag"])
    }
    triggers {
      cron('@daily')
    }
    stages {
      stage('Clean before building') {
        steps {
          deleteDir()
        }
      }
      stage('Build port') {
        steps {
          checkout scm
          dir ('lastArtifacts') {
            script {
              copyArtifacts(projectName: "${JOB_NAME}",
                            selector: lastSuccessful(),
                            optional: true)
            }
          }
          sh 'mv ./lastArtifacts/used-repos.txt last-used-repos.txt || true'
          dir ('scripts') {
            git(branch: 'main',
                url: 'https://gitlab.com/ubports/community-ports/jenkins-ci/halium-build-tools.git')
            sh """#!/bin/bash -xe
              unset USE_CCACHE
              pushd /opt/ubp-5.1/
                repo sync -v -c --force-sync --force-remove-dirty --no-prune
              popd
              if ./check-diff.sh '$WORKSPACE' '/opt/ubp-5.1/'; then
                echo 'check-diff detected the same build as last time, no need to build any more.'
                mv $WORKSPACE/lastArtifacts/device_*_devel.tar.* '$WORKSPACE'
                mv $WORKSPACE/lastArtifacts/ubp* '$WORKSPACE'
                exit 0
              fi
              pushd /opt/ubp-5.1/
                rm -fv $WORKSPACE/device_*_devel.tar.*
                rm -fv $WORKSPACE/ubp*
                make clean
                source build/envsetup.sh
                lunch '$lunchName-userdebug'
                time nice make -j8
              popd

              ./phablet-tarball.sh '$deviceName' '$WORKSPACE' '$deviceDir' '/opt/ubp-5.1/'
            """
          }
        }
      }
    }
    post {
      always {
        archiveArtifacts(artifacts: 'last-used-repos.txt,used-repos.txt',
                         fingerprint: true,
                         onlyIfSuccessful: false,
                         allowEmptyArchive: true)
        archiveArtifacts(artifacts: artifacts, fingerprint: true, onlyIfSuccessful: true)
      }
      success {
        script {
          if (currentBuild?.getPreviousBuild()?.resultIsWorseOrEqualTo("UNSTABLE")) {
            notifyTelegram("Phablet build for device $deviceName: FIXED")
          }
        }
      }
      unstable {
        notifyTelegram("Phablet build for device $deviceName: UNSTABLE, check ${JOB_URL}")
      }
      failure {
        script {
          if (currentBuild?.getPreviousBuild()?.resultIsWorseOrEqualTo("FAILURE")) {
            notifyTelegram("Phablet build for device $deviceName: NOT FIXED, check ${JOB_URL}")
          } else {
            notifyTelegram("Phablet build for device $deviceName: FAILURE, check ${JOB_URL}")
          }
        }
      }
      cleanup {
        deleteDir()
      }
    }
  }
}

def notifyTelegram(String message) {
  withCredentials([usernamePassword(credentialsId: 'a25d8b20-4a81-43e9-ac37-dcfb5285790a', usernameVariable: 'TELEGRAM_BOT_CREDS_USR', passwordVariable: 'TELEGRAM_BOT_CREDS_PWD')]) {
    env['TELEGRAM_BOT_MSG'] = message
    sh('curl -s -X POST https://api.telegram.org/$TELEGRAM_BOT_CREDS_PWD/sendMessage -d chat_id=$TELEGRAM_BOT_CREDS_USR -d text="$TELEGRAM_BOT_MSG"')
  }
}
